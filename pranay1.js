const express = require('express')
const app = express()
const port = process.env.PORT || 3000


const cors = require ('cors')


app.use(cors())

app.get('/pranay', (req, res) => {
    let some_database_object = {
        count: 4,
        list: [
            { name: 'pranay' },
            { name: 'sujeet' },
            { name: 'sushil' },
            { name: 'omkar' }
        ]
    }
    some_database_object.list = some_database_object.list.filter(item => item.name.includes(req.query.search || ''))
    res.json(some_database_object);
   
 
})

app.listen(port, () =>{
    console.log(`Example app listening on port ${port}`);
})